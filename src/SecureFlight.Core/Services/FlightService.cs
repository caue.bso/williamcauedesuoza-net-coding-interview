﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using System.Threading.Tasks;

namespace SecureFlight.Core.Services
{
    public class FlightService : BaseService<Flight>, IFlightService
    {
        private readonly IPassengerFlightRepository _passangerFlightRepository;

        public FlightService(IRepository<Flight> repository, IPassengerFlightRepository passangerFlightRepository) : base(repository)
        {
            _passangerFlightRepository = passangerFlightRepository;
        }

        public async Task<OperationResult<PassengerFlight>> AddPassanger(long flightId, string passangerId)
        {
            try
            {
                var result = await _passangerFlightRepository.Insert(new PassengerFlight { FlightId = flightId, PassengerId = passangerId });
                return new OperationResult<PassengerFlight>(result);
            }
            catch (Exception ex)
            {
                var error = new Error
                {
                    Code = ErrorCode.InternalError,
                    Message = ex.Message
                };

                return new OperationResult<PassengerFlight>(error);
            }
        }

        public async Task<OperationResult> RemovePassanger(long flightId, string passangerId)
        {
            try
            {
                await _passangerFlightRepository.Remove(new PassengerFlight { FlightId = flightId, PassengerId = passangerId });
                return new OperationResult(true);
            }
            catch (Exception ex)
            {
                var error = new Error
                {
                    Code = ErrorCode.InternalError,
                    Message = ex.Message
                };

                return new OperationResult(error);
            }
        }
    }
}
