﻿using SecureFlight.Core.Entities;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IPassengerFlightRepository
    {
        Task<PassengerFlight> Insert(PassengerFlight passengerFlight);
        Task Remove(PassengerFlight passengerFlight);
        //Task<bool> HasAnyFlight(string passengerId);
    }
}
