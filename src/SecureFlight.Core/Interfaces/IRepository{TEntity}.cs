﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IRepository<TEntity>
        where TEntity : class
    {
        Task<IReadOnlyList<TEntity>> GetAllAsync();

        TEntity Update(TEntity entity);
    }
}
