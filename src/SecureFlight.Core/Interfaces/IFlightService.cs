﻿using SecureFlight.Core.Entities;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IFlightService : IService<Flight>
    {
        Task<OperationResult<PassengerFlight>> AddPassanger(long flightId, string passangerId);
        Task<OperationResult> RemovePassanger(long flightId, string passangerId);
    }
}
