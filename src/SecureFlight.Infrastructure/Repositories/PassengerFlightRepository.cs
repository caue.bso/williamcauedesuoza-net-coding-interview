﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System.Threading.Tasks;

namespace SecureFlight.Infrastructure.Repositories
{
    public class PassengerFlightRepository : IPassengerFlightRepository
    {
        private readonly SecureFlightDbContext _context;

        public PassengerFlightRepository(SecureFlightDbContext context)
        {
            _context = context;
        }

        //public Task<bool> HasAnyFlight(string passengerId)
        //{
        //    //_context.
        //    return true;
        //}

        public async Task<PassengerFlight> Insert(PassengerFlight passengerFlight)
        {
            await _context.AddAsync(passengerFlight);
            await _context.SaveChangesAsync();
            return passengerFlight;
        }

        public async Task Remove(PassengerFlight passengerFlight)
        {
            _context.Remove(passengerFlight);
            await _context.SaveChangesAsync();
        }
    }
}
