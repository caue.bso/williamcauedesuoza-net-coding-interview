using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace SecureFlight.Test
{
    public class AirportTests : IClassFixture<WebApplicationFactory<Api.Startup>>
    {
        readonly HttpClient _client;
        public AirportTests(WebApplicationFactory<Api.Startup> application)
        {
            _client = application.CreateClient();
        }   

        [Fact]
        public async Task Update_Succeeds()
        {
            var response = await _client.GetAsync("/Airports");
            var body = await response.Content.ReadAsStringAsync();
            Assert.True(true);
        }
    }
}
